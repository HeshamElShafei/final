/** @author Hesham ElShafei
	@file f_main.cc
	     */



#include <iostream>
#include <string>


using namespace std;

/** @breif Prints a parameter and the word Lines after it
	@param string word1
	@return returns number of characters in the sentence
	     */
int countLine(string word1) {
	string man(word1 + " Lines");
	cout << man << endl;
	return man.length();
}
/** @breif Prints a parameter and the word Characters after it
	@param string input
	@return returns number of characters in the sentence
	     */
int countChar(string input) {
	string man(input + " Characters");
	cout << man<< endl;
	return man.length();
}
/** @breif Main function
	     */
int main() {
	cout << countLine("Ohio University");
	cout << endl << countChar("Athens") << endl;
}
