CC = g++

all: count doc check clean

count: f_main.cc
	$(CC) $(CFLAGS) f_main.cc -o count

doc: f_main.cc
	doxygen -g
	doxygen Doxyfile

check:
	valgrind --leak-check=full ./count
	../../gitread/cppcheck/cppcheck . --check-config	
clean: 
	rm -rf a.out Doxyfile latex html
